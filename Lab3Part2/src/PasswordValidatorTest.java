/*Aksenov Vladislav*/
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PasswordValidatorTest {

	@Test
	void testValidatePassword() {
		Boolean result = PasswordValidator.ifEnoughDigits("teston121");
		assertEquals(true, result);
	}
	
	
	
	@Test
	void exceptionalTestValidatePassword() {
		Boolean result = PasswordValidator.ifEnoughDigits("t");
		assertEquals(false, result);
	}
	
	
	@Test
	void boundaryInTestValidatePassword() {
		Boolean result = PasswordValidator.ifEnoughDigits("teston12");
		assertEquals(true, result);
	}
	
	@Test
	void boundaryOutTestValidatePassword() {
		Boolean result = PasswordValidator.ifEnoughDigits("teston1");
		assertEquals(false, result);
	}

}
