/*Aksenov Vladislav*/
public class PasswordValidator {
	/*There will be a method that allows to validate given password. rules are the following
	  A password must have at least 8 characters
	  A password must contain at least two digits*/
	public static Boolean ifCorrectLength(String string) {
		if (string.length() < 8)
			return false;
		return true;
	}
	
	public static Boolean ifEnoughDigits(String string) {
		int counter = 0;
		for (int i = 0; i < string.length(); ++i)
		{
			if (string.charAt(i) >= '0' && string.charAt(i) <= '9')
				counter++;
		}
		return counter >= 2;
	}
}
